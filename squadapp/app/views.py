from django.shortcuts import render
from django.views.decorators.csrf import csrf_protect
from django.http import HttpResponse, JsonResponse
from .forms import CityForm
from .models import Cities
import json
from pprint import pprint


# Create your views here.
@csrf_protect
def insert_new_city(request):
    form = CityForm(request.POST or None)
    if form.is_valid():
        city_name = form.cleaned_data['city_name']
        state = form.cleaned_data['state']
        city_name = ''.join(e for e in city_name if e.isalpha() or e.isspace()).lower()

        city_check = Cities.objects.filter(city_name=city_name, state=state)
        if not city_check:
            new_city = Cities(city_name=city_name, state=state)
            new_city.save()
            form = CityForm()

        else:
            return HttpResponse('This combination of city and state already exists')


    context = {
                'form': form
    }

    return render(request, "insert_city.html", context)

def search_city(request):
    data = request.body
    json_request = json.loads(data)
    payload = json_request['payload']

    res = []
    for dict in payload:
        city_name = dict['city_name']
        state = dict['state']
        city_tag = city_name + '/' + state

        city_name = ''.join(e for e in city_name if e.isalpha() or e.isspace()).lower()
        city_check = Cities.objects.filter(city_name=city_name, state=state)

        if city_check:
            print('city_check >>>>>', city_check)
            res.append({city_tag: 'Exists in database'})

        else:
            res.append({city_tag: 'Does not exists in database'})

    pprint(res)


    return JsonResponse(res, safe=False)
