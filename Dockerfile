FROM python:3.6
ENV PYTHONUNBUFFERED 1
RUN mkdir /src;
WORKDIR /src
ADD ./ ./
RUN pip install -r ./requirements.txt
RUN python3 ./manage.py migrate